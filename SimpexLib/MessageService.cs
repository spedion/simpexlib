﻿using SimpexLib.SoapServiceUtils;
using Spedion.SimpexLib;
using System;
using System.Net;
using System.ServiceModel;

namespace SimpexLib
{
    public class MessageService : MessageServiceSoapClient
    {
        private NetworkCredential _credentials = new NetworkCredential
        {
            UserName = ConfigStore.Instance.Configuration["MessageServiceUsername"],
            Password = ConfigStore.Instance.Configuration["MessageServicePassword"]
        };

        public MessageService(string url) : base(BindingFactory.GetHttpBindingWithBasicAuth(), new EndpointAddress(url))
        {
            SetSoapCredentials(Credentials);
            Endpoint.EndpointBehaviors.Add(new SpedionEndpointBehavior());
        }

        public MessageService(BasicHttpBinding binding, EndpointAddress address) : base(binding, address)
        {

        }

        public NetworkCredential Credentials
        {
            get => _credentials;
            set
            {
                _credentials = value;
                SetSoapCredentials(_credentials);
            }
        }

        private void SetSoapCredentials(NetworkCredential netCredentials)
        {
            ClientCredentials.UserName.UserName = netCredentials.UserName;
            ClientCredentials.UserName.Password = netCredentials.Password;
        }

        public static MessageService CreateWithCredentials()
        {
            string user = ConfigStore.Instance.Configuration["MessageServiceUsername"];
            string pwd = ConfigStore.Instance.Configuration["MessageServicePassword"];

            if (string.IsNullOrWhiteSpace(user) || user == "fill" || string.IsNullOrWhiteSpace(pwd) || pwd == "fill")
            {
                throw ErrorException();
            }

            return Create(user, pwd);
        }

        public static MessageService Create(string user, string pwd, string url = "https://simpex.spedion.de/simpex/3.1/Services/MessageService.asmx")
        {
            var config = ConfigStore.Instance.Configuration;
            if (!string.IsNullOrWhiteSpace(user))
            {
                config["MessageServiceUsername"] = user;
            }
            if (!string.IsNullOrWhiteSpace(pwd))
            {
                config["MessageServicePassword"] = pwd;
            }
            if (!string.IsNullOrWhiteSpace(url))
            {
                config["MessageServiceUrl"] = url;
            }

            return ServiceClientFactory.CreateMessageServiceClient();
        }

        private static Exception ErrorException()
        {
            return new ArgumentException("Please fill user settings in applicationSettings (App.config/Web.config/appsettings.json), or use Create() instead of CreateWithCredentials. Check Readme-SimpexLib.txt");
        }
    }

}
