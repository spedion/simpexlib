﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;

namespace SimpexLib.SoapServiceUtils
{
    public class Configuration : IConfiguration
    {
        private readonly Dictionary<string, string> _config = new Dictionary<string, string>();

        public IConfigurationSection GetSection(string key)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IConfigurationSection> GetChildren()
        {
            throw new NotImplementedException();
        }

        public IChangeToken GetReloadToken()
        {
            throw new NotImplementedException();
        }

        public string this[string key]
        {
            get => _config.TryGetValue(key, out var item) ? item : null;
            set => _config[key] = value;
        }
    }
}