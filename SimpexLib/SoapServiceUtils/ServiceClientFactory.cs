﻿using Spedion.SimpexLib;
using System.Net;

namespace SimpexLib.SoapServiceUtils
{
    public static class ServiceClientFactory
    {
        private static readonly SingleSoapServiceClientFactory<MessageService, MessageServiceSoap> MessageServiceFactory;

        static ServiceClientFactory()
        {
            var config = ConfigStore.Instance.Configuration;
            MessageServiceFactory = new SingleSoapServiceClientFactory<MessageService, MessageServiceSoap>(
                (address, binding) => new MessageService(binding, address),
                config["MessageServiceUrl"], false, true);
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
        }


        public static MessageService CreateMessageServiceClient()
        {
            return MessageServiceFactory.GetOrCreateServiceClient();
        }
    }
}
