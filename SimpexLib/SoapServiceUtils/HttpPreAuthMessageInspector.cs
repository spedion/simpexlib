﻿using System;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace SimpexLib.SoapServiceUtils
{
    internal class HttpPreAuthMessageInspector : IClientMessageInspector
    {
        private readonly ClientCredentials _credentials;

        public HttpPreAuthMessageInspector(ClientCredentials credentials)
        {
            _credentials = credentials;
        }

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {

        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            HttpRequestMessageProperty httpRequestMessage;
            if (request.Properties.TryGetValue(HttpRequestMessageProperty.Name, out var httpRequestMessageObject))
            {
                httpRequestMessage = httpRequestMessageObject as HttpRequestMessageProperty;
                SetAuthHeader(httpRequestMessage);
            }
            else
            {
                httpRequestMessage = new HttpRequestMessageProperty();
                SetAuthHeader(httpRequestMessage);
                request.Properties.Add(HttpRequestMessageProperty.Name, httpRequestMessage);
            }

            if (httpRequestMessage != null)
            {
                var version = "3.1";
                try
                {
                    version = Assembly.GetAssembly(typeof(HttpPreAuthMessageInspector)).GetName().Version.ToString();
                }
                catch
                {
                    // ignored
                }

                var userAgent = $"SimpexLib {version} {System.Runtime.InteropServices.RuntimeInformation.FrameworkDescription}";
                httpRequestMessage.Headers[HttpRequestHeader.UserAgent] = userAgent;
            }

            return null;
        }

        private void SetAuthHeader(HttpRequestMessageProperty httpRequestMessage)
        {
            if (httpRequestMessage == null) return;

            httpRequestMessage.Headers[HttpRequestHeader.Authorization] = "Basic " +
                                                                          Convert.ToBase64String(Encoding.ASCII.GetBytes(_credentials.UserName.UserName + ":" +
                                                                                                                         _credentials.UserName.Password));
        }
    }
}
