﻿using System;
using System.Collections.Concurrent;
using System.ServiceModel;

namespace SimpexLib.SoapServiceUtils
{
    public class SingleSoapServiceClientFactory<TServiceClientType, TClientBaseType> where TClientBaseType : class where TServiceClientType : ClientBase<TClientBaseType>
    {
        private readonly ConcurrentDictionary<string, TServiceClientType> _serviceInstancesByEndpointAddress = new ConcurrentDictionary<string, TServiceClientType>();

        private readonly Func<EndpointAddress, BasicHttpBinding, TServiceClientType> _createServiceClientFunction;
        private readonly string _endpointAddress;
        private readonly bool _useShorterTimeouts;
        private readonly bool _enableBasicAuth;
        private readonly Action<BasicHttpBinding> _optionalBindingModificationAction;
        private readonly object _creationLock = new object();

        public SingleSoapServiceClientFactory(Func<EndpointAddress, BasicHttpBinding, TServiceClientType> createServiceClientFunction,
            string endpointAddress,
            bool useShorterTimeouts,
            bool enableBasicAuth,
            Action<BasicHttpBinding> optionalBindingModificationAction = null)
        {
            _createServiceClientFunction = createServiceClientFunction;
            if (endpointAddress == null)
            {
                throw new Exception($"No endpoint Address specified for ServiceClient {typeof(TServiceClientType)}");
            }

            _endpointAddress = endpointAddress;
            _useShorterTimeouts = useShorterTimeouts;
            _enableBasicAuth = enableBasicAuth;
            _optionalBindingModificationAction = optionalBindingModificationAction;
        }

        public TServiceClientType GetOrCreateServiceClient(string endpointAddress = null)
        {
            string url = endpointAddress ?? _endpointAddress;
            TServiceClientType instance = null;

            lock (_creationLock)
            {
                instance = _serviceInstancesByEndpointAddress.AddOrUpdate(url, CreateServiceClient,
                    (newUrl, _) => CreateServiceClient(newUrl));
            }
            return instance;
        }

        private TServiceClientType CreateServiceClient(string endpointAddress = null)
        {
            var binding = CreateHttpBinding(_useShorterTimeouts);
            if (_enableBasicAuth)
            {
                binding.Security = new BasicHttpSecurity
                {
                    Mode = BasicHttpSecurityMode.Transport,
                    Transport = new HttpTransportSecurity { ClientCredentialType = HttpClientCredentialType.Basic, }
                };
            }

            if (_optionalBindingModificationAction != null)
            {
                _optionalBindingModificationAction(binding);
            }

            var client = _createServiceClientFunction(new EndpointAddress(endpointAddress ?? _endpointAddress), binding);
            if (!client.Endpoint.EndpointBehaviors.Contains(typeof(SpedionEndpointBehavior)))
            {
                client.Endpoint.EndpointBehaviors.Add(new SpedionEndpointBehavior());
            }

            if (_enableBasicAuth)
            {
                client.ClientCredentials.UserName.UserName = ConfigStore.Instance.Configuration["MessageServiceUsername"];
                client.ClientCredentials.UserName.Password = ConfigStore.Instance.Configuration["MessageServicePassword"];
            }

            return client;
        }

        private BasicHttpBinding CreateHttpBinding(bool useShorterTimeouts)
        {
            if (useShorterTimeouts)
            {
                return CreateHttpBindingWithShorterTimeouts();
            }

            return CreateDefaultHttpBinding();
        }


        private static BasicHttpBinding CreateDefaultHttpBinding()
        {
            return BindingFactory.GetHttpBinding();
        }

        private static BasicHttpBinding CreateHttpBindingWithShorterTimeouts()
        {
            var binding = CreateDefaultHttpBinding();
            binding.OpenTimeout = new TimeSpan(0, 0, 20);
            binding.CloseTimeout = new TimeSpan(0, 0, 20);
            binding.ReceiveTimeout = new TimeSpan(0, 0, 20);
            binding.SendTimeout = new TimeSpan(0, 0, 20);
            return binding;
        }
    }
}
