﻿using System.Net;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace SimpexLib.SoapServiceUtils
{
    public class SpedionEndpointBehavior : IEndpointBehavior
    {
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {

        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            ServicePoint servicePoint = ServicePointManager.FindServicePoint(endpoint.Address.Uri);
            servicePoint.Expect100Continue = false;
            servicePoint.ConnectionLimit = 100;

            //Debugging Encoding
            ServicePoint debuggingServicePoint = ServicePointManager.FindServicePoint(endpoint.Address.Uri, new WebProxy("127.0.0.1:8888"));
            debuggingServicePoint.Expect100Continue = false;
            debuggingServicePoint.ConnectionLimit = 100;

            ClientCredentials credentials = (ClientCredentials)endpoint.EndpointBehaviors[typeof(ClientCredentials)];

            if (credentials?.UserName?.UserName != null)
            {
                clientRuntime.ClientMessageInspectors.Add(new HttpPreAuthMessageInspector(credentials));
            }
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {

        }

        public void Validate(ServiceEndpoint endpoint)
        {

        }
    }
}
