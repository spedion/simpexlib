﻿using System.ServiceModel;

namespace SimpexLib.SoapServiceUtils
{
    public class BindingFactory
    {

        public static BasicHttpBinding GetHttpBinding()
        {
            var b = new BasicHttpBinding(BasicHttpSecurityMode.None)
            {
                MaxBufferSize = int.MaxValue,
                ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max,
                MaxReceivedMessageSize = int.MaxValue,
                AllowCookies = true,
            };

            return b;
        }

        public static BasicHttpBinding GetHttpBindingWithBasicAuth()
        {
            var b = GetHttpBinding();
            b.Security = new BasicHttpSecurity
            {
                Mode = BasicHttpSecurityMode.TransportCredentialOnly,
                Transport = new HttpTransportSecurity { ClientCredentialType = HttpClientCredentialType.Basic }
            };
            return b;
        }

        public static BasicHttpBinding GetHttpsBindingWithBasicAuth()
        {
            var b = GetHttpBinding();
            b.Security = new BasicHttpSecurity
            {
                Mode = BasicHttpSecurityMode.Transport,
                Transport = new HttpTransportSecurity { ClientCredentialType = HttpClientCredentialType.Basic }
            };
            return b;
        }
    }
}
