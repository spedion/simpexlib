﻿using Microsoft.Extensions.Configuration;
using System;

namespace SimpexLib.SoapServiceUtils
{
    public class ConfigStore
    {

        private static readonly Lazy<ConfigStore> Lazy = new Lazy<ConfigStore>(() => new ConfigStore());

        public static ConfigStore Instance => Lazy.Value;

        public IConfiguration Configuration { get; set; }

        private ConfigStore()
        {
            Configuration = new Configuration();
        }
    }
}
