﻿# Hi Simpex User!

## Documentation
The complete documentation is available at:
[Simpex documentation](https://simpex.spedion.de/simpex/3.1/Docu.aspx)

## The namespace
    using SimpexLib.MessageService;

## setting up password every time from your own config
    MessageService svc = MessageService.Create("yourUser", "yourPassword", ["yourUrl"]);
    // optional yourUrl defaults to https://simpex.spedion.de/simpex/3.1/Services/MessageService.asmx

## using user from config below

Please remember to setup appsettings.json to be copied into the build directory.

    IConfiguration configuration = new ConfigurationBuilder()
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddEnvironmentVariables()
        .Build();
    ConfigStore.Instance.Configuration = configuration;

This configuration reading should be placed in a centralized startup code.

    MessageService msgSvc = MessageService.CreateWithCredentials();

    But you have to setup the credentials in appsettings.json like this:

    {
        "MessageServiceUrl": "https://simpex.spedion.de/simpex/3.1/Services/MessageService.asmx",
        "MessageServiceUsername": "XXX - ENTER your user HERE - XXX",
        "MessageServicePassword":  "XXX - ENTER your password HERE - XXX"
    }

## Example usage
    int count = msgSvc.GetMessageCount();
    string version = msgSvc.GetVersion();