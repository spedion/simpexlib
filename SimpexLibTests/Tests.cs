using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using SimpexLib;
using SimpexLib.SoapServiceUtils;

namespace SimpexLibTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.test.json")
                .Build();
            ConfigStore.Instance.Configuration = config;
        }

        [Test]
        public void TestVersion()
        {
            var client = MessageService.CreateWithCredentials();
            var version = client.GetVersion();
            Assert.That(version, !Is.Empty);
        }
    }
}